from urllib.parse import urlparse, urlunparse
import requests, sys, os
from bs4 import BeautifulSoup


def crawl(url, depth, maxdepth, visited):
    if depth is 0:
        print("Crawling from " + url +  " to maximum depth of " + str(maxdepth) + " links")
    if depth < maxdepth:
        rootParse = urlparse(url)
        try:
            soup = BeautifulSoup(requests.get(url, timeout=1).text, 'html.parser').find_all('a', href=True)
            for link in soup:
                parse = urlparse(link.get('href'))
                if parse.fragment is not '':
                    parse = urlparse(urlunparse((parse[:5] + ('',))))
                if parse.scheme is '':
                    parse = urlparse(urlunparse((rootParse[0],) + parse[1:]))
                if parse.netloc is '':
                    parse = urlparse(urlunparse((parse[0],) + (rootParse[1],) + parse[2:]))
                if parse.scheme != 'http' and parse.scheme != 'https':
                    raise Exception
                if parse not in visited:
                    visited.append(parse)
                    print('\t' * depth + str(parse.geturl()))
                    crawl(parse.geturl(), depth + 1, maxdepth, visited)
        except requests.exceptions.RequestException and Exception:
            pass


if len(sys.argv) < 2:
    print("Error: no URL supplied")
else:
    try:
        requests.get(sys.argv[1], timeout=5)
    except:
        print("Error: Invalid URL supplied.\nPlease supply an absolute URL to this program")
        sys.exit(1)

    if len(sys.argv) < 3:
        crawl(sys.argv[1], 0, 3, [urlparse(sys.argv[1])])
    else:
        crawl(sys.argv[1], 0, int(sys.argv[2]), [urlparse(sys.argv[1])])
